#include "Box.h"
using namespace BoxAndContainerClasses;

	Box::Box() {
		lenght = 1;
		width = 1;
		height = 1;
		value = 1;
		weight = 1.0;
	}
	Box::Box(int len, int wid, int hei, double wei, int val) {
		if (len <= 0 || wid <= 0 || hei <= 0 || wei <= 0 || val <= 0) {
			err(1);
		}
		else {
			lenght = len;
			width = wid;
			height = hei;
			weight = wei;
			value = val;
		}
	}

	int Box::getLen() const { return lenght; }
	int Box::getWid() const { return width; }
	int Box::getHei() const { return height; }
	int Box::getVal() const { return value; }
	double Box::getWei() const { return weight; }

	Box::Box(const Box& box) {
		lenght = box.getLen();
		width = box.getWid();
		height = box.getHei();
		value = box.getVal();
		weight = box.getWei();
	}

	void Box::setLen(int& x) { lenght = x; }
	void Box::setWid(int& x) { width = x; }
	void Box::setHei(int& x) { height = x; }
	void Box::setVal(int& x) { value = x; }
	void Box::setWei(double& x) { weight = x; }
 
	ostream& BoxAndContainerClasses::operator<<
		(ostream& os, const Box& box) {
		os << box.getLen() << " " << box.getWid() << " "
			<< box.getHei() << " " << box.getWei() << " " 
			<< box.getVal()<< "\n";
		return os;
	}

	istream& BoxAndContainerClasses::operator>>
		(istream& is, Box& box) {
		is >> box.lenght >> box.height >> box.width >> box.weight >> box.value;
		return is;
	}
