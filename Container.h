#pragma once
#include <iostream>
#include <vector>
#include "Box.h"

using namespace std;

namespace BoxAndContainerClasses {
	class Container
	{
	private:
		int lenght = -1, width = -1, height = -1;
		double maxWeight = -1.0;
		vector<Box> boxes;
		//Container();
	public:
		
		Container(int len, int wid, int hei, double max_wei);
		Container(Container& c);
		int getLenght() const;
		int getHeight() const;
		int getWidth() const;
		double getMaxWeight() const;
		vector<Box> &getBoxes();
		int countOfBoxes();
		double sumWeight();
		double costOfAll();
		bool isEmpty();
		bool isFull();
		Box getBox(int index);
		int addBox(Box box);
		void addBoxI(int i, Box boxI);
		void removeBoxI(int i);
		friend ostream& operator<<(ostream& os, Container& c);//const
		friend istream& operator>>(istream& is, Container& c);
		Box& operator[](int index);
	};
}
