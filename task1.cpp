﻿#include <iostream>
#include "Box.h"
#include "Container.h"

using namespace std;
using namespace BoxAndContainerClasses;

int main()
{
	
	//Box* box1 = new Box(-5, 1, 1, 1.0, 1);
	//cout << *box1 << "\n";
	//delete box1;
	//cout << "------------------------------------------------\n";
	Box box2 = Box(5, 1, 1,  1.01, 1);
	//Box* box3 = new Box(5, 1, 4, 1.01, 1);

	//cout << box2;
	//cin >> box2;
	//cout << box2;
   // cout << "\n";
	
	cout << "--------------------Container-------------------------\n";
	Container* c1 = new Container(4, 5, 6, 150);
	//cout<< "capacity: " <<(c1)->getBoxes().capacity() << "\n";
	try {
		cout << "addBox: " << c1->addBox(box2) << "\n";
	}
	catch (exception ex) {
		cout << "exception: " << ex.what() << "\n";
	}
	//cout << c1->getBox(0) << "\n";
	//cout <<"weight: "<< c1->sumWeight() << "\n";
	//cout << "cost: " << c1->costOfAll() << "\n";
	//cout << *c1;
	//cout<< "Count of boxes: " << c1->countOfBoxes();
	//c1->removeBoxI(0);
	//c1->addingBoxI(0, *box2);
	//c1->addingBoxI(1, *box3);
	//cout << *c1;
	delete c1;
}

