#pragma once
#include <iostream>

using namespace std;
namespace BoxAndContainerClasses {
	class Box
	{
	private: int lenght, width, height, value;
			 double weight;

			 void err(int err) {
				 if (err == 1) {
					 cout << "Uncorrectly value.\n";

				 }
				 else if (err == 2) {
					 cout << "Array isn't exist.\n";

				 }
				 else {
					 cout << "Unknown error\n";
				 }

			 }
			 
	public:
		Box();
		Box(int len, int wid, int hei, double wei, int val);
		Box(const Box& box);

		int getLen() const;
		int getWid() const;
		int getHei() const;
		int getVal() const;
		double getWei() const;
		void setLen(int& x);
		void setWid(int& x);
		void setHei(int& x);
		void setVal(int& x);
		void setWei(double& x);

		friend ostream& operator<<(ostream& os, const Box& box);
		friend istream& operator>>(istream& is, Box& box);
	};

}