#include "Container.h"

using namespace BoxAndContainerClasses;

	void err(int err) {
		if (err == 1) {
			cout << "Incorrectly values\n";
		}
		else if (err == 2) {
			cout << "Element i isn't empty\n";
		}
		else if (err == 3) {
			cout << "Incorrectly index\n";
		}
		else if (err == 4) {
			cout << "Container is empty.\n";
		}
		else if (err == 5) {
			cout << "Container is full.\n";
		}
		else if (err == 6) {
			cout << "This box can't be pushed because it is more that max weight\n.";
		}
		else if (err == 7) {
			cout << "Index is out of bound.\n";
		}
		else {
			cout << "Unknown error\n";
		}
	}
/*
Container::Container() {
	//boxes.reserve(10);
	lenght = 1;
	width = 1;
	height = 1;
	maxWeight = 1000;
	boxes.resize(5);
}*/
Container::Container(int len, int wid, int hei, double max_wei) {
	if (len <= 0 || wid <= 0 || hei <= 0 || max_wei <= 0) {
		err(1);
	}
	else {
		lenght = len;
		width = wid;
		height = hei;
		maxWeight = max_wei;
	}
}

int Container::getLenght() const { return this->lenght; }

int Container::getHeight() const { return this->height; }

int Container::getWidth() const {	return this->width; }

double Container::getMaxWeight() const { return this->maxWeight; }

vector<Box> &Container::getBoxes() {	return this->boxes; }

Container::Container(Container& container) {
	boxes = container.getBoxes();
	lenght = container.getLenght();
	width = container.getWidth();
	height = container.getHeight();
	maxWeight = container.getMaxWeight();
}

int Container::countOfBoxes() { return getBoxes().size(); }

double Container::costOfAll() {
	double sum = 0.0;
	for (int i = 0; i < countOfBoxes(); i++) {
		sum += (this->boxes[i]).getVal();
	}
	return sum;
}

Box Container::getBox(int index) {
	if (index < 0 || index >= countOfBoxes()) {
		err(3);
	}
	else if (isEmpty()) {
		err(4);
	}
	else {
		return this->boxes[index];
	}
}

double Container::sumWeight() {
	double sum = 0.0;
	for (int i = 0; i < countOfBoxes(); i++) {
		sum += (this->boxes[i]).getWei();
	}
	return sum;
}

bool Container::isEmpty() {
	bool res = false;
	if (countOfBoxes() == 0) {
		res = true;
	}
	return res;
}

bool Container::isFull() {
	bool res = false;
	if (sumWeight() >= getMaxWeight()) {
		res = true;
	}
	return res;
}

 int Container::addBox(Box box) {
	if (isFull()) {
		err(5);
	}
	else if (getMaxWeight() <= sumWeight() + box.getWei()) {
		err(6);
		throw exception("TOO_BIG_WEIGHT");
	}
	else {
		this->boxes.push_back(box);
		return countOfBoxes();
	}
 }

 void Container::addBoxI(int index, Box boxI) {
	 if (isFull()) {
		 err(5);
	 }
	 else if (getMaxWeight() <= sumWeight() + boxI.getWei()) {
		 err(6);
	 }
	 else {
		 vector<Box>::iterator it;
		 it = this->boxes.begin();
		 this->boxes.insert(it + index, boxI);
	 }
 }

 void Container::removeBoxI(int i) {
	 if (isEmpty()) {
		 err(4);
	 }
	 else if (countOfBoxes() < i || i < 0) {
		 err(7);
	 }
	 else {
		 vector<Box>::iterator it;
		 it = this->boxes.begin();
		 this->boxes.erase(it + i);
	 }
 }

 ostream& BoxAndContainerClasses::operator<<
	 (ostream& os, Container& c)
 {
	 os << "height: " << c.getHeight() << "\n"
		 << "weight: " << c.getMaxWeight() << "\n"
		 << "width: " << c.getWidth() << "\n"
		 << "lenght: " << c.getLenght() << "\n"
		 << "count of boxes: " << c.countOfBoxes() << "\n";
	 return os;
 }

 
 istream& BoxAndContainerClasses::operator>>
	 (istream& is, Container& c) {
	is >> c.lenght >> c.width >> c.height >> c.maxWeight;
	return is;
}

 Box& Container::operator[](int index) {
	 return this->boxes[index];
 }